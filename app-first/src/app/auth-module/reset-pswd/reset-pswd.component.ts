import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-reset-pswd',
  templateUrl: './reset-pswd.component.html',
  styleUrls: ['./reset-pswd.component.css'],
})
export class ResetPswdComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dataServ: DataService
  ) {}

  secretToken!: string;
  logProcessMessage!: string;
  showMessage: boolean = false;

  confirmPassword!: string;
  password!: string;

  ngOnInit(): void {
    console.log('Reset password run');
    this.secretToken = this.route.snapshot.params['token'];
    console.log(this.secretToken);
    if (!this.route.snapshot.params['token']) {
      this.router.navigate(['/']);
    }
    localStorage.clear(); //deleting the user in local storage
    this.dataServ.isLogged = false;
    this.dataServ.userStatus.emit(false);
  }

  resetPassword() {
    if (this.password !== this.confirmPassword || this.password == '') {
      this.showMessage = true;
      this.logProcessMessage = "Confirm password and password don't match";
    } else {
      this.showMessage = true;
      this.logProcessMessage = 'Sending request to reset password';
      this.dataServ
        .resetPassword({ token: this.secretToken, pswd: this.password })
        .subscribe({
          next: (data: any) => {
            this.logProcessMessage = data.body.info;
            console.log('Generate token request recieved');
            console.log(data);
            this.router.navigate(['/auth/login']);
          },
          error: (err) => {
            console.log('Generate token Error');
            console.log(err);
            this.logProcessMessage = err.error.info;
          },
        });
    }
  }
}
