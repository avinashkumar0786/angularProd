import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthRoutingModule} from "./auth-routing.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {LoginComponent} from "./login/login.component";
import {RegistrationComponent} from "./registration/registration.component";
import {ResetPswdComponent} from "./reset-pswd/reset-pswd.component";
import {ForgotPswdComponent} from "./forgot-pswd/forgot-pswd.component";
import {AuthComponent} from "./auth.component";
import {GoogleLoginProvider, SocialAuthServiceConfig} from "angularx-social-login";
import {environment} from "../../environments/environment";
import {HttpClientModule} from "@angular/common/http";
import { VerifyOtpComponent } from './verify-otp/verify-otp.component';


@NgModule({
  declarations: [
    AuthComponent,
    LoginComponent,
    RegistrationComponent,
    ResetPswdComponent,
    ForgotPswdComponent,
    VerifyOtpComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],

  exports:[CommonModule]
})
export class AuthModuleModule { }
