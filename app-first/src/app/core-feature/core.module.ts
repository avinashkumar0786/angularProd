import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlowComponent } from './flow/flow.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TestComponent } from './test/test.component';
import { CoreFeatureComponent } from './core-feature.component';
import { CoreRoutingModule } from './core-routing.module';
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    FlowComponent,
    DashboardComponent,
    TestComponent,
    CoreFeatureComponent,
  ],
  imports: [CommonModule, CoreRoutingModule,FormsModule],
  exports: [CommonModule],
})
export class CoreModule {}
