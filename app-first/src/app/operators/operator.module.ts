import { NgModule } from '@angular/core';
import { SubjectComponent } from './subject/subject.component';
import { ReplaySubjectComponent } from './replay-subject/replay-subject.component';
import { DebounceComponent } from './debounce/debounce.component';
import { CommonModule } from '@angular/common';
import { OperatorRoutingModule } from './operator-routing.module';
import { OperatorsComponent } from './operators.component';
import { MapsComponent } from './maps/maps.component';
import { FormsModule } from '@angular/forms';
import { TakeComponent } from './take/take.component';
import { RetryComponent } from './retry/retry.component';

@NgModule({
  declarations: [
    SubjectComponent,
    ReplaySubjectComponent,
    DebounceComponent,
    OperatorsComponent,
    MapsComponent,
    TakeComponent,
    RetryComponent,
  ],
  imports: [CommonModule, OperatorRoutingModule, FormsModule],
  exports: [CommonModule],
})
export class OperatorModule {}
