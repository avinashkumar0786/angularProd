import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OperatorsComponent } from './operators.component';
import { SubjectComponent } from './subject/subject.component';
import { MapsComponent } from './maps/maps.component';
import { TakeComponent } from './take/take.component';
import { RetryComponent } from './retry/retry.component';

const routes: Routes = [
  {
    path: '',
    component: OperatorsComponent,
    children: [
      {
        path: 'subject',
        component: SubjectComponent,
      },
      {
        path: 'maps',
        component: MapsComponent,
      },
      {
        path: 'take',
        component: TakeComponent,
      },
      {
        path: 'retry',
        component: RetryComponent,
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OperatorRoutingModule {}
