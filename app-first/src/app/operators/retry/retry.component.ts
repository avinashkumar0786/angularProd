import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { delay, retry, retryWhen, scan } from 'rxjs/operators';

@Component({
  selector: 'app-retry',
  templateUrl: './retry.component.html',
  styleUrls: ['./retry.component.css'],
})
export class RetryComponent implements OnInit {
  constructor(private dataServ: DataService) {}

  ngOnInit(): void {
    console.log('Retry operator, init');
  }
  name!: string;
  email!: string;
  userName!: string;
  info: string = 'NO-Data';
  fetchingData: boolean = false;
  fetchDetails() {
    this.fetchingData = true;
    this.info = 'Fetching Data';
    this.dataServ
      .loadProfile()
      .pipe(
        // retry(4),
        retryWhen((err: any) =>
          err.pipe(
            delay(3000),
            scan((ret) => {
              if (ret > 3) {
                this.fetchingData = false;
                this.info = 'Retry Failed';
                throw err;
              } else {
                console.log('Retry: ' + ret);
                this.info = 'Retry: ' + ret;
                ret++;
                return ret;
              }
            }, 1)
          )
        )
      )
      .subscribe(
        (data: any) => {
          console.log(data.body);
          this.name = data.body.fname + ' ' + data.body.lname;
          this.email = data.body.email;
          this.userName = data.body.userName;
          this.fetchingData = false;
          this.info = 'Fetching Success';
        },
        (error) => {
          this.info = 'Error Fetching Data';
          console.log('Error fetching data');
          console.log(error);
        }
      );
  }
}
