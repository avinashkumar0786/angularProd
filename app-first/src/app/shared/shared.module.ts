import { NgModule } from '@angular/core';
import { ErrorPageComponent } from './error-page/error-page.component';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    ErrorPageComponent,
    LoadingSpinnerComponent,
  ],
  imports: [CommonModule],
  exports: [
    ErrorPageComponent,
    LoadingSpinnerComponent,
    CommonModule,
  ],
})
export class SharedModule {}
