import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './shared/home-page/home-page.component';
import { AccessGuardService } from './services/access-guard.service';
import {NotFoundComponent} from "./shared/not-found/not-found.component";

const appRoutes: Routes = [
  { path: '', component: HomePageComponent, canActivate: [AccessGuardService] },
  {
    path: 'resume',
    loadChildren: () =>
      import('./resume-module/resume-module.module').then((mod) => mod.ResumeModuleModule),
  },
  {
    path: 'dashboard',
    loadChildren: () =>
      import('./dashboard-module/dashboard-module.module').then((mod) => mod.DashboardModuleModule),
  },
  {
    path: 'profile',
    loadChildren: () =>
      import('./profile-module/profile-module.module').then((mod) => mod.ProfileModuleModule),
  },
  {
    path: 'auth',
    loadChildren: () =>
      import('./auth-module/auth-module.module').then((mod) => mod.AuthModuleModule),
  },
  {
    path: 'operator',
    loadChildren: () =>
      import('./operators/operator.module').then((mod) => mod.OperatorModule),
  },
  {
    path: 'widgets',
    loadChildren: () =>
      import('./widgets/widgets.module').then((mod) => mod.WidgetsModule),
  },
  { path: 'not-found', component: NotFoundComponent },
  { path: '**', redirectTo: '/not-found' },
];
@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
