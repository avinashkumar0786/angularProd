import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AddressmapComponent} from "./addressmap/addressmap.component";
import { DynDropdownComponent } from './dyn-dropdown/dyn-dropdown.component';
import { PayUmoneyComponent } from './pay-umoney/pay-umoney.component';
import { WidgetsComponent } from './widgets.component';

const routes:Routes=[
  {
    path:'',
    component:WidgetsComponent,
    children:[
      {path:"addressMap",component:AddressmapComponent},
      {path:"payUmoney",component:PayUmoneyComponent},
      {path:"dynDropdown",component:DynDropdownComponent}

    ]
  }
]
@NgModule({
  imports:[RouterModule.forChild(routes)],
  exports:[RouterModule]
})   
export class WidgetRoutingModule {}
