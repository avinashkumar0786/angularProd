import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dyn-dropdown',
  templateUrl: './dyn-dropdown.component.html',
  styleUrls: ['./dyn-dropdown.component.css']
})
export class DynDropdownComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  updateValue(){
    console.log("Parse called");
    try {
      this.iterateOver()
    } catch (error) {
      console.log("Error parsing data");
      console.error(error)
    }

  }
  nestedArray:any=[['Group A', 'Group B', 'Group C'],['Option A1', 'Option A2'],['Option A2-1', 'Option A2-2', 'Option A2-3'],['Option A2-3-1', 'Option A2-3-2', 'Option A2-3-3']];
  splChar="=";
  parentStack:{}[]=[];
  finalDropobj:{}={};
  curParentLevel=0;
  parentRefrence:any=this.finalDropobj;
  nestedArraySelected:string[]=[];
  getKeysFromObj(index?:number){
    console.log("getKeysFromObj called");
    return ["apple","mango","sasas"];
  }
  initNestedArray(){
    this.nestedArray.forEach((ele:any) => {
      this.nestedArraySelected.push(ele[0])
    });
  }
  ngAfterViewInit(){
    this.initNestedArray()
  }
  onChange(lev:any,val:any){
    if(!Object.keys(this.finalDropobj).length){
      console.log("Object not parsed, provide input");
      return;
    }
    console.log("change lev: ",lev," value: ",val);
    // console.log(" slice after the curr index");
    // console.log(this.nestedArraySelected);
    this.nestedArray=this.nestedArray.slice(0,lev+1);
    this.nestedArraySelected=this.nestedArraySelected.slice(0,lev+1);
    let tempRef:any=this.finalDropobj;
    let i=0;
    //getting refrence to current select
    for(let j=0;j<this.nestedArraySelected.length;j++){
      tempRef=tempRef[this.nestedArraySelected[j]];
      if(!tempRef) break;
      // (tempRef?console.log(Object.keys(tempRef)):"");
    }
    // console.log("Temp ref: ",tempRef);
    //pushing nest options and selcting first of all options
    while(tempRef && Object.keys(tempRef).length){
      // console.log(Object.keys(tempRef));
      this.nestedArray.push(Object.keys(tempRef));
      // console.log("cur selected: ",this.nestedArraySelected[i]);
      let nextSelect=Object.keys(tempRef)[0];
      this.nestedArraySelected.push(nextSelect);
      tempRef=tempRef[nextSelect];
    }
  }
  
  // parentStack.push(parentRefrence);
  inpString="";
  iterateOver(){
    // console.log(this.inpString)
    this.curParentLevel=0;
    this.parentStack=[];
    this.finalDropobj={};
    this.parentStack.push(this.finalDropobj);
    this.parentRefrence=this.finalDropobj;
    let prevSkip=0;
    for(let i=0;i<this.inpString.length;i++){
      if (this.inpString.charAt(i) == this.splChar){
        let skip = this.countLevel(i); i = i + skip
        let word=this.getWord(i).trim();
        
        // console.log("SPl found len:",skip,word," cur lev: ",this.curParentLevel);
        if(skip>this.curParentLevel){
          // console.log("**PUSH***child found, inside nesting",word);
          // console.log(parentStack)
          this.parentRefrence[word]={};
          this.parentStack.push(this.parentRefrence[word]);
          this.parentRefrence = this.parentRefrence[word];
          skip=this.curParentLevel+1;
          // console.log(parentRefrence)
        }
        else if (skip < this.curParentLevel){
          // console.log("*****Next Parent found, outsde nesting");
          let pops=this.curParentLevel-skip;
          // console.log(parentStack)
          while (this.parentStack.length &&  pops>=0){
            this.parentStack.pop();
            pops--;
          }
          this.parentRefrence=this.parentStack.slice(-1)[0];
          this.parentRefrence[word]={};
          this.parentStack.push(this.parentRefrence[word]);
          this.parentRefrence = this.parentRefrence[word];
          // console.log(parentRefrence)
          // console.log(parentStack)
        }
        else{
          // console.log("*****siblng  found:", parentRefrence);
          // console.log(parentStack);
          this.parentStack.pop();// remove prev sibling and ins obj
          this.parentRefrence=this.parentStack.slice(-1)[0];
          this.parentRefrence[word]={};
          this.parentStack.push(this.parentRefrence[word]) ////pushing cur blank object
          this.parentRefrence = this.parentRefrence[word];
        }
        this.curParentLevel=skip;
        prevSkip=skip;
        // console.log("-----------------------")
      }
    }
    //updating nested array, 
    console.log(this.finalDropobj);
    this.updateNestedArray();
  }

  updateNestedArray(){
    this.nestedArray=[];
    let iterObj:any=this.finalDropobj;
    console.log('updating nested array and selected array to init value');
    while(iterObj && Object.keys(iterObj).length){
      // console.log(Object.keys(tempRef));
      this.nestedArray.push(Object.keys(iterObj));
      // console.log("cur selected: ",this.nestedArraySelected[i]);
      let nextSelect=Object.keys(iterObj)[0];
      this.nestedArraySelected.push(nextSelect);
      iterObj=iterObj[Object.keys(iterObj)[0]];
    };
    console.log(this.nestedArray);
    console.log(this.nestedArraySelected)
  }
  // utils
  countLevel(index:number){
    let count=0;
    while(this.inpString.charAt(index)==this.splChar){
      index++;
      count++;
    }
    return count;
  }
  getWord(index:number){
    let word = this.inpString.slice(index).split("=")[0];
    return word.replace('\\n',"");
  }
}
