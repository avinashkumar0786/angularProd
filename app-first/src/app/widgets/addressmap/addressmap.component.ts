import { Component, ElementRef, OnInit ,ViewChild} from '@angular/core';

@Component({
  selector: 'app-addressmap',
  templateUrl: './addressmap.component.html',
  styleUrls: ['./addressmap.component.css']
})
export class AddressmapComponent implements OnInit {
  map!: google.maps.Map;
  infoWindow = new google.maps.InfoWindow();

  constructor() { }
  ngOnInit(): void {
    console.log("Address map init init");
    
  }
  @ViewChild('mapContainer', { static: false })
  gmap!: ElementRef;
  lat = 31.250317176709427;
  lng = 75.71906821494142;
  coordinates = new google.maps.LatLng(this.lat, this.lng);

  mapOptions: google.maps.MapOptions = {
    center: this.coordinates,
    zoom: 12,
  };
  marker = new google.maps.Marker({
    position: this.coordinates,
    map: this.map,
    draggable: true,
    animation: google.maps.Animation.DROP,
  });
  curPosLat:any=0;
  curPosLong:any=0;
  country:String="";
  state:String="";
  local:String="";
  mapInitializer() {
    this.map = new google.maps.Map(this.gmap.nativeElement, this.mapOptions);
    const locationButton = document.createElement("button");

    locationButton.textContent = "Locate to Current Location";
    // locationButton.classList.add(" btn btn-info");
    this.map.controls[google.maps.ControlPosition.TOP_CENTER].push(locationButton);
    locationButton.addEventListener("click", () => {
      this.trackCurloc()
    });
    this.map.addListener("click", (e) => {
      console.log("map clicked ",e.latLng);
      this.marker.setPosition(e.latLng);
      this.map.panTo(e.latLng);
      this.curPosLat=e.latLng.lat()?e.latLng.lat():this.curPosLat;
      this.curPosLong=e.latLng.lng()?e.latLng.lng():this.curPosLong;

      this.getGeoData(new google.maps.Geocoder(),this.map,this.infoWindow);   //getting location data reverse geoCoding
    });
    this.marker.setMap(this.map);
    this.marker.addListener("mouseup", () => {
      console.log("Marker dropped");
      console.log("lat: ",this.marker.getPosition()?.lat()," long: ",this.marker.getPosition()?.lng());
      this.curPosLat=this.marker.getPosition()?.lat()?this.marker.getPosition()?.lat():this.curPosLat;
      this.curPosLong=this.marker.getPosition()?.lng()?this.marker.getPosition()?.lng():this.curPosLong;

      this.marker.setPosition( new google.maps.LatLng( this.curPosLat,this.curPosLong) );
      this.getGeoData(new google.maps.Geocoder(),this.map,this.infoWindow);   //getting location data reverse geoCoding
    });
  }
  ngAfterViewInit() {
    console.log("Opening map");
    this.mapInitializer();
  }
  trackCurloc(){
    navigator.permissions && navigator.permissions.query({name: 'geolocation'})
    .then(function(PermissionStatus) {
        if (PermissionStatus.state == 'granted') {
              console.log("allowed")
        } else if (PermissionStatus.state == 'prompt') {
          console.log("prompt")
        } else {
          console.log("Location access denied")
        }
    })
    
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };
          console.log(pos)
          this.curPosLat=position.coords.latitude;
          this.curPosLong=position.coords.longitude;
          this.infoWindow.setPosition(pos);
          this.infoWindow.setContent("Current location.");
          this.infoWindow.open(this.map);
          this.map.setCenter(pos);
          this.marker.setPosition( new google.maps.LatLng( pos.lat, pos.lng) );
          this.map.panTo( new google.maps.LatLng( pos.lat, pos.lng ) );
          this.getGeoData(new google.maps.Geocoder(),this.map,this.infoWindow); //getting location data reverse geoCoding
        (error:any) => {
          console.log("Location access NOT provided");
          this.handleLocationError(true, this.infoWindow, this.map.getCenter());
        }}
      )
    } else {
      // Browser doesn't support Geolocation
      console.log("Location access NOT provided");
      this.handleLocationError(false, this.infoWindow, this.map.getCenter());
    }
    
  }



  getGeoData(
    geocoder: google.maps.Geocoder,
    map: google.maps.Map,
    infowindow: google.maps.InfoWindow,
  ) {
    console.log("Getting geo dta");
    const latlng = {
      lat: this.curPosLat,
      lng:  this.curPosLong,
    };
  
    geocoder.geocode({ location: latlng },(response:any) => {
      console.log(":::::::::::::::::::Data recieved::::::::::::::::::");
      console.log(response);
      if (response && response.length) {
        // map.setZoom(15);
        let respLen=response.length-1;
        infowindow.setContent(response[0]?.formatted_address);
        this.country=response[respLen]?.formatted_address;
        this.state=response[respLen-1]?.formatted_address;
        this.local=response[respLen-2]?.formatted_address;
        infowindow.open(map, this.marker);
      } else {
        console.log("No results found");
      }
    })
  }

  handleLocationError(browserHasGeolocation:any, infoWindow:any, pos:any) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(
      browserHasGeolocation
        ? "Error: The Geolocation service failed."
        : "Error: Your browser doesn't support geolocation."
    );
    infoWindow.open(this.map);
  }



}
