import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressmapComponent } from './addressmap/addressmap.component';
import {WidgetRoutingModule} from "./widgets.routing.module"
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { WidgetsComponent } from './widgets.component';
import { PayUmoneyComponent } from './pay-umoney/pay-umoney.component';
import { DynDropdownComponent } from './dyn-dropdown/dyn-dropdown.component';
import {MatSelectModule} from '@angular/material/select';


@NgModule({
  declarations: [
    AddressmapComponent,
    WidgetsComponent,
    PayUmoneyComponent,
    DynDropdownComponent
  ],
  imports: [
    CommonModule,
    WidgetRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    HttpClientModule
  ]
})
export class WidgetsModule { }
