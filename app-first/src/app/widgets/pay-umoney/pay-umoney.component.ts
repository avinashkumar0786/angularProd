import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-pay-umoney',
  templateUrl: './pay-umoney.component.html',
  styleUrls: ['./pay-umoney.component.css']
})
export class PayUmoneyComponent implements OnInit {

  constructor(private http:HttpClient) { }

  ngOnInit(): void {
    console.log("Pay u money init")
  }
  merchantkey:string="";
  merchantSalt:string="";
  transactionId:string="";
  amount:string="";
  email:string="";
  firstName:string="";
  productInfo:string="";
  // hashSequence = key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5||||||salt.

  //  pay u international
  payUData:{}={
    "notifyUrl": "https://your.eshop.com/notify",
    "customerIp": "127.0.0.1",
    "merchantPosId": "300746",
    "description": "RTV market",
    "currencyCode": "PLN",
    "totalAmount": "21000",
    "buyer": {
        "email": "john.doe@example.com",
        "phone": "654111654",
        "firstName": "John",
        "lastName": "Doe",
        "language": "pl"
    },
    "products": [
        {
            "name": "Wireless Mouse for Laptop",
            "unitPrice": "15000",
            "quantity": "1"
        },
        {
            "name": "HDMI cable",
            "unitPrice": "6000",
            "quantity": "1"
        }
    ]
  }
  payUoptions(){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer d081b7c4-c30f-46c0-bdad-1bb5b38b1688',
      'Access-Control-Allow-Origin':"*"
    });
    return Object.assign({ headers: headers, method: 'post' }, { withCredentials: true});
  }
  initpayu(){
    console.log("pay u init called");
    let payUlink:any="https://secure.snd.payu.com/api/v2_1/orders";

    this.http.post(payUlink, this.payUData, this.payUoptions())
    .subscribe({
      next: (data: any) => {
        console.log('data recieved Profile');
        console.log(data)
      },
      error: (err) => {
        console.log(err);
        console.log('error occured in payu ');
      }});
  }
}
