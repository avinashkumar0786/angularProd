import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-template1',
  templateUrl: './template1.component.html',
  styleUrls: ['./template1.component.css'],
})
export class Template1Component implements OnInit {
  constructor() {}

  ngOnInit(): void {
    console.log('Template one init');
  }
  showResume: boolean = true;
  @Input() inpResumeData!: any;
}
