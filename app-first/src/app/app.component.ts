import { Component, SimpleChanges } from '@angular/core';
import { DataService } from './services/data.service';
import { ActivatedRoute, Router, RouterStateSnapshot } from '@angular/router';
import { SocialAuthService } from 'angularx-social-login';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  constructor(
    private dataService: DataService,
    private router: Router,
    private actRoute: ActivatedRoute
  ) {
    console.log('App Component:  constructor called');
  }

  ngOnInit() {
    console.log('App Component:  NG-ON-init called');
    console.log('User auth key: ' + localStorage.getItem('auth'));
    console.log(this.router.url);
    this.dataService.dataChannel.subscribe((data: any) => {
      console.log('Data recieved form next');
      console.log(data);
    });
  }

  //---------Code for assignments------------------

  //---------Code for assignments------------------

  yourName = 'Avinash Kumar';
  title = '';
  rootValue = 'My ROOT value from app component';

  serverElements = [
    {
      type: 'server',
      name: 'My test server',
      content: 'This is my description',
    },
    {
      type: 'blueprint',
      name: 'My production server',
      content: 'This is my production server',
    },
  ];
  changeRootVal() {
    this.rootValue = 'Changed value';
  }

  selectedTab: string = '';
  onNavigate(feature: string) {
    console.log(feature);
    this.selectedTab = feature;
  }
}
