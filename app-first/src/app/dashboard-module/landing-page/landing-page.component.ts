import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css'],
})
export class LandingPageComponent implements OnInit {
  constructor(private dataServ: DataService, private _snackBar: MatSnackBar) {}

  ngOnInit(): void {}
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action);
  }
}
