import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatSidenavModule} from '@angular/material/sidenav';
import {LandingPageComponent} from "./landing-page/landing-page.component";
import {DashboardRoutingModule} from "./dashboard.routing.module";
import { SideNavBarComponent } from './side-nav-bar/side-nav-bar.component';
import {DashboardComponent} from "./dashboard.component";
import {DemoMaterialModule} from './material.module';
import { Feature01Component } from './feature01/feature01.component';
@NgModule({
  declarations: [
    LandingPageComponent,
    SideNavBarComponent,
    DashboardComponent,
    Feature01Component
  ],
  imports: [
    CommonModule,
    DemoMaterialModule,
    DashboardRoutingModule,
    MatSidenavModule
  ],
  exports:[CommonModule]
})
export class DashboardModuleModule { }
