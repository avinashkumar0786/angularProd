import {Component, OnInit, ViewChild} from '@angular/core';
import { DataService } from '../services/data.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard.component.html',
  styleUrls:['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  constructor(private dataService: DataService,private router: Router) {}
  ngOnInit(): void {}
  showFiller = false;
  sideBarOpen:boolean=false;
  @ViewChild('drawer') drawer: any;
  toggleSideBar(){
    this.drawer.toggle();
    this.sideBarOpen=!this.sideBarOpen;
  }


  logoutUser() {
    console.log('Logging OUT user');
    this.dataService.logoutUser().subscribe({
      next: (data) => {
        console.log('Logged out user');
        console.log(data);
        localStorage.clear(); //deleting the user in local storage
        this.dataService.isLogged = false;
        this.dataService.isMobileVerified=false;//setting mobile verification to true
        this.dataService.userStatus.emit(false);
        this.router.navigate(['/auth/login']);
      },
      error: (err) => {
        console.log(err);
      },
    });
  }
}
