var express = require("express");
var router = express.Router();
var secureController = require("../controllers/secureNg");
var multer = require("multer");
var request = require('request');

//for pdf parse
let fs = require("fs");
const emailUtil = require("../email-util");
const { STATUS_CODES } = require("http");
const { sendEmail } = emailUtil;
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./temp/fileUploads");
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});
var upload = multer({ storage: storage, limits: { fileSize: 100000 } });
router.get('/paymentResponse',(req,res)=>{
  console.log("payment continue")
  console.log(req.body);
  res.status(200).send({
    header:req.header,
    body:req.body
  })
})
router.get('/paymentNotify',(req,res)=>{
  console.log("payment notify")
  console.log(req.body);
  res.status(200).send({
    header:req.header,
    body:req.body
  })
})
router.get('/payuOauth',(req,res)=>{
  var accessTokenOptions = {
    'method': 'POST',
    'url': 'https://secure.snd.payu.com/pl/standard/user/oauth/authorize?grant_type=client_credentials&client_id=300746&client_secret=2ee86a66e5d97e3fadc400c9f19b065d',
    'headers': {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Cookie': 'cookieFingerprint=54818ef5-c5eb-4ebc-a8b8-2ef97af40ffc; payu_persistent=mobile_agent-false#'
    },
    form: {
  
    }
  };
  request(accessTokenOptions, function (error, response) {
    if (error) throw new Error(error);
    response.body=JSON.parse(response.body.toString().replace("\\",""));
    console.log(response.body);
    res.json(
      response.body,
    )
  });
})
router.get("/",(req,res)=>{
  res.json({
    info:"secure#main"
  })
})
router.get("/payUpayment", (req, res) => {
  console.log("Req for main secure");
  var accessTokenOptions = {
    'method': 'POST',
    'url': 'https://secure.snd.payu.com/pl/standard/user/oauth/authorize?grant_type=client_credentials&client_id=300746&client_secret=2ee86a66e5d97e3fadc400c9f19b065d',
    'headers': {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Cookie': 'cookieFingerprint=54818ef5-c5eb-4ebc-a8b8-2ef97af40ffc; payu_persistent=mobile_agent-false#'
    },
    form: {
  
    }
  };
  request(accessTokenOptions, function (error, response) {
    if (error) throw new Error(error);
    response.body=JSON.parse(response.body.toString().replace("\\",""));
    console.log(response.body);
    // res.json(
    //   response.body,
    // )
    var options = {
      'method': 'POST',
      'url': 'https://secure.snd.payu.com/api/v2_1/orders',
      'headers': {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+response.body.access_token,
        'Cookie': 'cookieFingerprint=54818ef5-c5eb-4ebc-a8b8-2ef97af40ffc; payu_persistent=mobile_agent-false#'
      },
      body: JSON.stringify({
        "notifyUrl": "http://localhost:3000/secure/paymentNotify",
        "continueUrl":"http://localhost:3000/secure/paymentResponse",
        "customerIp": "127.0.0.1",
        "merchantPosId": "300746",
        "description": "Bhuppi sir da dhaba",
        "currencyCode": "PLN",
        "totalAmount": "99999",
        "buyer": {
          "email": "bhupinder.singh@venturepact.com",
          "phone": "654111654",
          "firstName": "Bhuppinder",
          "lastName": "Singh Raz",
          "language": "pl"
        },
        "products": [
          {
            "name": "i phone 13 max pro",
            "unitPrice": "15000",
            "quantity": "1"
          },
          {
            "name": "HDMI cable",
            "unitPrice": "6000",
            "quantity": "1"
          }
        ]
      })
  
    };
    request(options, function (error, paymentResponse) {
      if (error) throw new Error(error);
      paymentResponse.body=JSON.parse(paymentResponse.body.toString().replace("\\",""));
      console.log(paymentResponse.body)
      res.json({
        response: "Pay u data",
        status: paymentResponse.body.status,
        orderid:paymentResponse.body.orderId,
        redirectUrl:paymentResponse.body.redirectUri
      });
    });
  });
});

//routes for widgets
router.get("/widgets", secureController.profile);

router.post("/register", secureController.registerUser);
router.post("/login", secureController.loginUser);
router.get("/checkUserId/:uid", secureController.checkUserName);
router.get("/profile", secureController.profile);
router.post("/update", secureController.updateProfile);
router.post("/googleLogin", secureController.googleOauthLogin);
router.post("/resetPassword", secureController.resetPassword);
router.post("/genResetToken", secureController.genResetToken);
router.post('/sendSMSotp',secureController.sendSMS);
router.post('/verifyOtp',secureController.verifyOtp);


//routes for the CV
router.get("/cvinfo", secureController.getCVdet);
router.post("/cvSingle", secureController.getOneCV);
router.post("/updateResume", secureController.updateResume);
router.post("/newResume", secureController.newResume);
router.post("/deleteResume", secureController.deleteResume);
router.post(
  "/fileUpload",
  // upload.single("myfile"),
  secureController.fileUpload
);

router.get("/logout", (req, res) => {
  console.log("cookies deleted");
  res.clearCookie("auth");
  console.log(req["cookies"]);
  res.status(200).json({
    logout: true,
  });
});

router.get("/isAuth", (req, res) => {
  jwt.verify(req.token, "mysecretKey", (err, authData) => {
    if (err) {
      console.log(err);
      res.sendStatus(404);
    } else {
      res.json({
        message: "Secure access",
        data: authData,
      });
    }
  });
});
router.get("/setCookie/:key", (req, res) => {
  res.cookie("value", req.params.key);
  res.send(req.cookies);
});

router.get("/getCookie", (req, res) => {
  console.log(req.cookies);
  res.send(req.cookies);
});

//testing the pdf parse
router.get("/parsePdf", (req, res) => {
  console.log("Parsing pdf start");
});

router.get("/sendMail", async (req, res) => {
  const { recipient, message } = req.body;
  try {
    console.log("Start sending mail");
    let ans = await sendEmail("recipient", "message");
    console.log(ans);
    res.json({ message: "Your query has been sent" });
  } catch (e) {
    console.log("Error sending mail");
    console.log(e);
  }
});
module.exports = router;

/*
let dataBuffer = fs.readFileSync("./temp/pdfFiles/resume.pdf");

  pdf(dataBuffer).then(function (data) {
    // number of pages
    console.log(data.numpages);
    // number of rendered pages
    console.log(data.numrender);
    // PDF info
    console.log(data.info);
    // PDF metadata
    console.log(data.metadata);
    // PDF.js version
    // check https://mozilla.github.io/pdf.js/getting_started/
    console.log(data.version);
    // PDF text
    console.log(data.text);
    res.send(data.text);
  });

  ////////seond way
  let i = 0;

  var rows = {}; // indexed by y-position
  new pdfreader.PdfReader().parseFileItems(
    "./temp/pdfFiles/resume.pdf",
    function (err, item) {
      if (!item || item.page) {
        // end of file, or page
        printRows();
        rows = {}; // clear rows for next page
      } else if (item.text) {
        (rows[item.y] = rows[item.y] || []).push(item.text);
      }
    }
  );
  function printRows() {
    console.log("printing values: " + i++);
    Object.keys(rows) // => array of y-positions (type: float)
      .sort((y1, y2) => parseFloat(y1) - parseFloat(y2)) // sort float positions
      .forEach((y) => console.log(`[${y}]: [ ${(rows[y] || []).join("")}]`));
    console.log("round: " + i);
    if (i === 2) res.json(rows);
  }


  //third way
  let dataBuffer = fs.readFileSync("./temp/pdfFiles/JONATHAN_MA_RESUME.pdf");

  pdf(dataBuffer).then(function (data) {
    // number of pages
    console.log(data.numpages);
    // number of rendered pages
    console.log(data.numrender);
    // PDF info
    console.log(data.info);
    // PDF metadata
    console.log(data.metadata);
    // PDF.js version
    // check https://mozilla.github.io/pdf.js/getting_started/
    console.log(data.version);
    // PDF text
    console.log(data.text);
    res.json(data.text);
  });
 */
